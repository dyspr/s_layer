var colors = {
  light: [13, 13, 13],
  dark: [0, 0, 0]
}

var boardSize
var maxSize = 0.4
var steps = 17

function setup() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function draw() {
  createCanvas(windowWidth, windowHeight)
  background(colors.light)
  rectMode(CENTER)
  colorMode(RGB, 255, 255, 255, 1)

  fill(colors.dark)
  noStroke()
  rect(windowWidth *  0.5, windowHeight * 0.5, boardSize, boardSize)

  for (var i = 0; i < steps; i++) {
    fill(255, 255, 255, 0.125 - 0.125 * abs(sin(Math.PI * frameCount * (1 / 100) + i * (2 / steps))))
    noStroke()
    push()
    translate(windowWidth * 0.5, windowHeight * 0.5 + (i - Math.floor(steps * 0.5)) * boardSize * maxSize * 0.075)
    drawSquare(boardSize * maxSize * (1 - (i / steps) * 0.3))
    pop()

  }

}

function windowResized() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function drawSquare(size) {
  beginShape()
  vertex(-0.6 * size, -0.4 * size)
  vertex(0.6 * size, -0.4 * size)
  vertex(size, 0.4 * size)
  vertex(-size, 0.4 * size)
  endShape(CLOSE)
}
